package ru.t1consulting.nkolesnik.tm.service;

import ru.t1consulting.nkolesnik.tm.api.repository.ITaskRepository;
import ru.t1consulting.nkolesnik.tm.api.service.ITaskService;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.entity.TaskNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.*;
import ru.t1consulting.nkolesnik.tm.model.Task;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository repository) {
        super(repository);
    }

    @Override
    public Task create(final String userId, final String name) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        return repository.create(userId, name);
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        return repository.create(userId, name, description);
    }

    @Override
    public Task create(
            final String userId,
            final String name,
            final String description,
            final Date dateBegin,
            final Date dateEnd
    ) {
        final Task task = Optional.ofNullable(create(userId, name, description))
                        .orElseThrow(TaskNotFoundException::new);
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        return task;
    }

    @Override
    public Task updateById(final String userId, final String id, final String name, final String description) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(id).orElseThrow(TaskIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        final Task task = Optional.ofNullable(findById(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final String userId, final Integer index, final String name, final String description) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(index).filter(idx->idx>-1).orElseThrow(IndexIncorrectException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        final Task task = Optional.ofNullable(findByIndex(userId, index))
                .orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final String userId, final Integer index, final Status status) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(index).filter(idx->idx>-1).orElseThrow(IndexIncorrectException::new);
        final Task task = Optional.ofNullable(findByIndex(userId, index))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String userId, final String id, final Status status) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(id).orElseThrow(TaskIdEmptyException::new);
        final Task task = Optional.ofNullable(findById(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(status);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(userId, projectId);
    }

}
