package ru.t1consulting.nkolesnik.tm.command.user;

import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractUserCommand {

    public static final String NAME = "registry";

    public static final String DESCRIPTION = "Registry new user.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[ENTER LOGIN]");
        final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD]");
        final String password = TerminalUtil.nextLine();
        System.out.println("[ENTER EMAIL]");
        final String email = TerminalUtil.nextLine();
        getAuthService().registry(login, password, email);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
