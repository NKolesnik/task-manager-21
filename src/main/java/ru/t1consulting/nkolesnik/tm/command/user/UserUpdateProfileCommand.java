package ru.t1consulting.nkolesnik.tm.command.user;

import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractUserCommand {

    public static final String NAME = "user-update-profile";

    public static final String DESCRIPTION = "Update user info.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[ENTER FIRST NAME]");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("[ENTER MIDDLE NAME]");
        final String middleName = TerminalUtil.nextLine();
        System.out.println("[ENTER LAST NAME]");
        final String lastName = TerminalUtil.nextLine();
        getUserService().updateUser(userId, firstName, middleName, lastName);
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
