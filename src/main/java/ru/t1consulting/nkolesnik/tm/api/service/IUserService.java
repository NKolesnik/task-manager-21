package ru.t1consulting.nkolesnik.tm.api.service;

import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.model.User;

public interface IUserService extends IService<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User create(String login, String password, String email, Role role);

    User findByLogin(String login);

    User findByEmail(String email);

    User setPassword(String id, String password);

    User updateUser(String id, String firstName, String middleName, String lastName);

    User removeByLogin(String login);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

}
